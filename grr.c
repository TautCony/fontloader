#include <windows.h>

#ifdef UNICODE
#define CommandLineToArgv  CommandLineToArgvW
#else
#define CommandLineToArgv  CommandLineToArgvA
#endif // !UNICODE

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
    int argc = 0;
    int actualCount = 0;
    LPWSTR* lpFonts;
    int loaded = 0;
    wchar_t message[256];
    int i = 1;

    lpFonts = CommandLineToArgv(GetCommandLine(), &argc);
    if (lpFonts == NULL)
    {
        wsprintf(message, L"Error code: %lu", GetLastError());
        MessageBox(NULL, message, L"Failed to read arguments", MB_OK | MB_ICONHAND);
        return -1;
    }
    if (argc < 2)
    {
        MessageBox(NULL, L"Drag and drop font file(s) onto .exe.", L"Usage", MB_OK | MB_ICONASTERISK);
        LocalFree(lpFonts);
        return -1;
    }

    for (i = 1; i < argc; ++i)
    {
        if (AddFontResource(lpFonts[i]) == 0)
        {
            MessageBox(NULL, lpFonts[i], L"Failed to load font", MB_OK | MB_ICONHAND);
            actualCount = i + 1;
            goto REMOVEFONTS;
        }
        ++loaded;
    }
    actualCount = argc;
    SendMessage(HWND_BROADCAST, WM_FONTCHANGE, 0, 0);
    wsprintf(message, L"%d fonts loaded successfully.", loaded);
    MessageBox(NULL, L"Close this box to unload fonts.", message, MB_OK | MB_ICONASTERISK);
REMOVEFONTS:
    for (i = 1; i < actualCount; ++i)
    {
        RemoveFontResource(lpFonts[i]);
    }
    SendMessage(HWND_BROADCAST, WM_FONTCHANGE, 0, 0);
    LocalFree(lpFonts);
    return 0;
}

void WinMainCRTStartup()
{
    ExitProcess(WinMain(GetModuleHandle(NULL), NULL, NULL, 0));
}
